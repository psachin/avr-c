/*
  ADC w/ interrupt
*/
#define F_CPU 16000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "uart.h"
#include "extras.h"

volatile uint8_t adcval;

ISR(ADC_vect) {
    adcval = ADCH;
}


int main(void) {
    DDRB |= (1<<PD3) | (1<<PD4) | (1<<PD5);		/* LED: pin 11, 12, 13*/
    PORTB = 0b00000000;		/* all pins OFF */

    /* UART stuff */
    uart_init(9600);
    // Assign our stream to standart I/O streams
    stdout=&usart0_str;


    ADMUX = (ADMUX & 0xF0) | (3 & 0x0F); /* In free running mode, always select
					  * the channel(A3 in this case) before
					  * the first conversion */

    ADMUX |= (1 << REFS0);    // use AVcc as the reference
    ADMUX |= (1 << ADLAR);    // Right adjust for 8 bit resolution

    ADCSRA |= (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0); // 128 prescale for 16Mhz
    ADCSRA |= (1 << ADATE);   // Set ADC Auto Trigger Enable

    /* Free running mode */
    ADCSRB = 0;

    /* Enable ADC */
    ADCSRA |= (1<<ADEN);

    /* Enable interrupt */
    ADCSRA |= (1<<ADIE);

    /* Start the conversion */
    ADCSRA |= (1<<ADSC);

    sei();			/* Activate interrupt */

    while(1) {
	if(adcval > 128) {
	    setbit(PORTB, PORTB3);
	} else {
	    clearbit(PORTB, PORTB3);
	}
	printf("adcval: %d \n", adcval);
	_delay_ms(1000);
    }
    return (0);
}
