void init_ADC(void) {
    /* Reference voltage */
    ADMUX = (1 << REFS0);

    /* 10 bit resolution */
    ADMUX &= ~(1<<ADLAR);

    /*
      If ADMUX |= (1<<ADLAR) then it is enough to read ADCH
    */

    /*
      Set Pre scaler at /128 and enable ADC
      16MHz/128=125KHz
     */
    ADCSRA = (1 << ADPS0) | (1 << ADPS1) | (1 << ADPS2);

    /* Enable ADC */
    ADCSRA |= (1 << ADEN);
}

uint16_t read_ADC(uint8_t channel) {
    uint8_t high, low;

    /* Select channel */
    ADMUX |= (ADMUX & 0xF0) | (channel & 0x0F);
    // ADMUX |= (1<<MUX1) | (0<<MUX0);  // A3

    /* Start ADC conversion */
    ADCSRA |= (1 << ADSC);

    /* Wait for conversion.
       ADIF is high after the conversion is completed
    */
    while(!(ADCSRA & (1<<ADIF)));

    // ADCSRA |= (1 << ADIF);   //reset as required

    low = ADCL;
    high = ADCH;
    return(high << 8 | low);
}
