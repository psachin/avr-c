/*
  lightSensor: Based on https://gitlab.com/psachin/induinor3/blob/master/lightSensor/lightSensor.ino
*/
#define F_CPU 16000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "uart.h"
#include "adc.h"
#include "extras.h"

int threshold1 = 350;
int threshold2 = 650;
int threshold3 = 950;

int main(void) {
    uint16_t adcval;
    init_ADC();
    uart_init(9600);
    stdout=&usart0_str;

    DDRB = 0b00111000;
    PORTB = 0b00000000;		/* all pins OFF */

    while(1) {
	adcval = read_ADC(3);
	printf("Light Intensity is: %d \n", adcval);
	printf("Size of adcval: %d \n", sizeof(adcval));
	if(adcval > threshold1) {
	    setbit(PORTB, 3);
	} else {
	    clearbit(PORTB, 3);
	}

	if(adcval > threshold2) {
	    setbit(PORTB, 4);
	} else {
	    clearbit(PORTB, 4);
	}

	if(adcval > threshold3) {
	    setbit(PORTB, 5);
	} else {
	    clearbit(PORTB, 5);
	}

	_delay_ms(500);		/* 500 ms */
    }
    return 0;
}
