/*
  ADC w/o interrupt
 */
#define F_CPU 16000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "uart.h"
#include "adc.h"
#include "extras.h"

int main(void) {
    uint16_t adcval;
    init_ADC();
    uart_init(9600);
    stdout=&usart0_str;

    DDRB = 0b00111000;
    PORTB = 0b00000000;		/* all pins OFF */

    while(1) {
	adcval = read_ADC(3);
	if(adcval > 600) {
	    setbit(PORTB, 3);
	} else {
	    clearbit(PORTB, 3);
	}
	printf("adcval: %d \n", adcval);
	_delay_ms(1000);
    }
    return 0;
}
