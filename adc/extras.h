#define setbit(port, bit) (port) |= (1 << bit)
#define clearbit(port, bit) (port) &= ~(1 << bit)
