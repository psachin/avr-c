#include <stdio.h>
#include <avr/io.h>

#define CPU_CLOCK 16000000UL	// 16 MHz clock is assumed
#define UBRR(baud) ((uint32_t)(CPU_CLOCK)/(uint32_t)(baud*16UL) - 1)


//Initialise UART: format 8 data bits, No parity, 1 stop bit
void uart_init(uint16_t baud) {
    /* Set baud rate */
    int ubrr = UBRR(baud);
    UBRR0H = (uint16_t) (ubrr >> 8);
    UBRR0L = (uint16_t) ubrr;

    /* Enable receiver & transmitter */
    UCSR0B = (1 << TXEN0) | (1 << RXEN0);

    /* Frame format: 8 data, No parity, 1 stop bit: 8N1 */
    // UCSR0C = (1 << UCSZ01) | (1 << UCSZ00) ;

    /* Frame format: 8 data, No parity, 2 stop bit: 8N1 */
    UCSR0C = (3 << UCSZ00) ;
}

uint8_t uart_rx(void) {
    /* Wait for data to be received */
    while ( !(UCSR0A & (1<<RXC0)) );

    /* Get and return received data from buffer */
    return UDR0;
}

void uart_tx(uint8_t c) {
    loop_until_bit_is_set(UCSR0A, UDRE0);

    /* Put data into buffer, sends the data */
    UDR0 = c;
}

void uart_send_string(char *s) {
    while(*s != 0x00) {
	uart_tx(*s);
	s++;
    }
}

int USART0SendByte(char u8Data, FILE *stream) {
    if(u8Data == '\n') {
        USART0SendByte('\r', stream);
    }
    // wait while previous byte is completed
    loop_until_bit_is_set(UCSR0A, UDRE0);
    // Transmit data
    UDR0 = u8Data;
    return 0;
}

//set stream pointer
FILE usart0_str = FDEV_SETUP_STREAM(USART0SendByte, NULL, _FDEV_SETUP_WRITE);
