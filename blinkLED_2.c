/*
  blinkLED_2: LED train
*/

/* Externel clock(quartz) speed you are using: 16MHz*/
#define F_CPU 16000000L
#include <avr/io.h>
#include <util/delay.h>

#define setbit(port, bit) (port) |= (1 << bit)
#define clearbit(port, bit) (port) &= ~(1 << bit)

int main(void) {

    /*
      - Digital pin 11: PB3 (pin 17 on IC)
      - Digital pin 12: PB4 (pin 18 on IC)
      - Digital pin 13: PB5 (pin 19 on IC)
    */
    int i;

    DDRB = 0b00111000;          /* Set digital pins 11, 12 & 3 as output */
    PORTB = 0b00000000;          /* All pins OFF */

    while(1) {
	for(i=3; i<6; i++) {
	    setbit(PORTB, i);
	    _delay_ms(65);
	    clearbit(PORTB, i);
	    _delay_ms(65);
	}
    }
}
