* ATMega328-PU[fn:datasheet] in pure avr-C
** Dependencies
   - gcc-avr
   - avr-libc
   - avrdude

** Usage
   - To compile
     #+BEGIN_SRC shell
       make compile ARG="hello"
       # OR
       make ARG="hello"
     #+END_SRC sh

     /Note/: In the above example if you provide the string as *hello*
     to =ARG=, you must have =hello.c= in the same directory as
     =Makefile=.

   - To flash/burn your program
     #+BEGIN_SRC sh
       make burn ARG="hello"
     #+END_SRC sh

   - To clean
     #+BEGIN_SRC sh
       make clean ARG="hello"
     #+END_SRC sh

     Optionally you can also run
     #+BEGIN_SRC sh
       make clean
     #+END_SRC sh

[fn:datasheet]: [[ATmega328-PU
Datasheet][http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega48A-PA-88A-PA-168A-PA-328-P-DS-DS40002061A.pdf]]
